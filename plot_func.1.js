var $ = $ || {}, d3 = d3 || {};

var functions = new Array;

function S(a,b,c){
    return function(x){
        if(x<=a) return 0;
        else if(x<=b) return Math.pow(x-a,2)/(2*Math.pow(b-a,2));
        else if(x<=c) return 1-Math.pow(x-c,2)/(2*Math.pow(b-c,2));
        else return 1;
    };
}

function SoftTri(a,b,c){
	var uphill = S(a,(a+b)/2,b), downhill = (x)=>(1-S(b,(b+c)/2,c)(x));
	return function(x){
		if (x<=a) return 0;
		else if(x<=b) return uphill(x);
		else if(x<=c) return downhill(x);
		else return 0;
	};
}

function SoftTrap(a,b1,b2,c){
	var uphill = S(a,(a+b1)/2,b1), downhill = (x)=>(1-S(b2,(b2+c)/2,c)(x));
	return function(x){
		if (x<=a) return 0;
		else if(x<=b1) return uphill(x);
		else if(x<=b2) return 1;
		else if(x<=c) return downhill(x);
		else return 0;
	};
}

function collapse(f, domain){
	var r=new Array;
    var x=d3.range(domain[0],domain[1],0.05), y=x.map(f);
    for(var i=0;i<x.length;++i) r.push({x:x[i], y:y[i]});
    functions.push(r);
}

(function(){
	// collapse(S(1,2,3,4), [-2,12]);
	// collapse(SoftTri(1,3,7), [-2,12]);
	collapse(SoftTrap(1,3,5,7), [-2,12]);
})();

function render_plots() {
    var view = { width: 1320, height: 750 };
	
	var svg = d3.select("body").append("svg")
		.attr("width", view.width)
		.attr("height", view.height);
		
    function render_plot0(margin){
	    var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		
		var x = d3.scaleLinear()
			.domain([-2,12])
		    .range([0, width]);
		
		var y = d3.scaleLinear()
			.domain([-0.2,1.2])
		    .range([height, 0]);
		
		var line = d3.line()
		    .defined(function(d) { return d; })
		    .x(function(d) { return x(d.x); })
		    .y(function(d) { return y(d.y); });
		    
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		container.append("g")
		    .attr("class", "axis axis--x")
		    .attr("transform", "translate(0," + height + ")")
		    .call(d3.axisBottom().scale(x));
		
		container.append("g")
		    .attr("class", "axis axis--y")
		    .call(d3.axisLeft().scale(y));
		
		container.append("g").selectAll(".line")
		    .data(functions)
			.enter().append("path")
		    .attr("class", "line")
		    .attr("stroke", "steelblue")
		  //  .attr("stroke", function(d, i) { return color_scale_v(i); } )
		    .attr("d", line);
    }
    
    render_plot0({top: 20, right: 660, bottom: 440, left: 40});
}

$(render_plots);
