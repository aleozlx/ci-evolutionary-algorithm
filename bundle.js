/*
Copyright 2016 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

Player = (function() {
    function Player() {
        this.timerIndex = 0,
        this.isPlaying = !1,
        this.callback = null
    }
    return Player.prototype.playOrPause = function() {
        this.isPlaying ? (this.isPlaying = !1,
        this.pause()) : (this.isPlaying = !0,
        this.play())
    },
    Player.prototype.onPlayPause = function(callback) {
        this.callback = callback
    },
    Player.prototype.play = function() {
        this.pause(),
        this.isPlaying = !0,
        this.callback && this.callback(this.isPlaying),
        this.start(this.timerIndex)
    },
    Player.prototype.pause = function() {
        this.timerIndex++,
        this.isPlaying = !1,
        this.callback && this.callback(this.isPlaying)
    },
    Player.prototype.start = function(localTimerIndex) {
        var _this = this;
        d3.timer(function() {
            return localTimerIndex < _this.timerIndex || (oneStep(),
            !1)
        }, 0)
    },
    Player
})();

var iter = 0, player = new Player;
function oneStep() {
    iter++;
    // trainData.forEach(function(point, i) {
    //     var input = constructInput(point.x, point.y);
    //     nn.forwardProp(network, input),
    //     nn.backProp(network, point.label, nn.Errors.SQUARE),
    //     (i + 1) % state.batchSize === 0 && nn.updateWeights(network, state.learningRate, state.regularizationRate)
    // }),
    // lossTrain = getLoss(network, trainData),
    // lossTest = getLoss(network, testData),
    updateUI();
}

function userHasInteracted(){
    
}

function makeGUI() {
    d3.select("#reset-button").on("click", function() {
        reset(),
        userHasInteracted(),
        d3.select("#play-pause-button")
    }),
    d3.select("#play-pause-button").on("click", function() {
        player.playOrPause(),
        userHasInteracted()
    });
    player.onPlayPause(function(isPlaying) {
        d3.select("#play-pause-button").classed("playing", isPlaying)
    })
}

function updateUI(firstStep) {
    function zeroPad(n) {
        var pad = "000000";
        return (pad + n).slice(-pad.length)
    }
    function addCommas(s) {
        return s.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    function humanReadable(n) {
        return n.toFixed(3)
    }
    void 0 === firstStep && (firstStep = !1),
    // updateWeightsUI(network, d3.select("g.core")),
    // updateBiasesUI(network),
    // updateDecisionBoundary(network, firstStep);
    // var selectedId = null != selectedNodeId ? selectedNodeId : nn.getOutputNode(network).id;
    // heatMap.updateBackground(boundary[selectedId], state.discretize),
    // d3.select("#network").selectAll("div.canvas").each(function(data) {
    //     data.heatmap.updateBackground(heatmap_1.reduceMatrix(boundary[data.id], 10), state.discretize)
    // }),
    // d3.select("#loss-train").text(humanReadable(lossTrain)),
    // d3.select("#loss-test").text(humanReadable(lossTest)),
    d3.select("#iter-number").text(addCommas(zeroPad(iter)))
    // lineChart.addDataPoint([lossTrain, lossTest])
}


makeGUI()
