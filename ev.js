var $ = $ || {}, d3 = d3 || {};

function render_plots() {
    var view = { width: 1100, height: 600 };
	
	var svg = d3.select("#demo-area").append("svg")
		.attr("width", view.width)
		.attr("height", view.height);
		
    function render_plot0(margin){
	    var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		
		var x = d3.scaleLinear()
			.domain([-2,12])
		    .range([0, width]);
		
		var y = d3.scaleLinear()
			.domain([-0.2,1.2])
		    .range([height, 0]);
		
		var line = d3.line()
		    .defined(function(d) { return d; })
		    .x(function(d) { return x(d.x); })
		    .y(function(d) { return y(d.y); });
		    
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		container.append("g")
		    .attr("class", "axis axis--x")
		    .attr("transform", "translate(0," + height + ")")
		    .call(d3.axisBottom().scale(x));
		
		container.append("g")
		    .attr("class", "axis axis--y")
		    .call(d3.axisLeft().scale(y));
		
		container.append("g").selectAll(".line")
		    .data(functions)
			.enter().append("path")
		    .attr("class", "line")
		    .attr("stroke", "steelblue")
		  //  .attr("stroke", function(d, i) { return color_scale_v(i); } )
		    .attr("d", line);
    }
    
    function show_problem(margin){
	    var width = view.width - margin.left - margin.right,
	    	height = view.height - margin.top - margin.bottom;
		
		var x = d3.scaleLinear()
			.domain([-2,12])
		    .range([0, width]);
		
		var y = d3.scaleLinear()
			.domain([-0.2,1.2])
		    .range([height, 0]);
		
		var line = d3.line()
		    .defined(function(d) { return d; })
		    .x(function(d) { return x(d.x); })
		    .y(function(d) { return y(d.y); });
		    
		var container = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		// Plot settings
		var start_point = {x:0, y:0}, end_point = {x:width-40, y:height/2};
		var translate_transform = "translate(20,20)";
		
		// Start point
		var g_start = container.append("g").attr("transform", translate_transform);
		g_start.append("circle").attr("class", "dot")
	      .attr("cx", start_point.x).attr("cy", start_point.y).attr("r", 6);
	    g_start.append("text").attr("y", start_point.y+3).attr("x", start_point.x-20).text("A");
	      
	    // End point
	    var g_end = container.append("g").attr("transform", translate_transform);
		g_end.append("circle").attr("class", "dot movable-v")
	      .attr("cx", end_point.x).attr("cy", end_point.y).attr("r", 6)
	      .call(d3.drag().on('drag', function(){
	      	  end_point.y = d3.event.y;
		   	  d3.select(this).attr("cy", end_point.y);
		   	  g_end.selectAll("text").attr("y", end_point.y);
		   	  redraw_solution([cycloid(end_point.x, end_point.y).data_points]);
		  }));
	    g_end.append("text").attr("y", end_point.y).attr("x", end_point.x+10).text("B");
	    
	    // Solution path
	    var line = d3.line()
		    .defined(function(d) { return d; })
		    .x(function(d) { return d.x; })
		    .y(function(d) { return d.y; });
		function test_curve(){
			return [{t:0,x:0,y:0},{t:1,x:end_point.x,y:end_point.y}];
		}
		function redraw_solution(all_curves){
			container.selectAll(".curves").remove();
		    container.append("g").attr("transform", translate_transform)
		    	.attr("class", "curves")
		    	.selectAll(".line")
			    .data(all_curves)
				.enter().append("path")
			    .attr("class", "line")
			    .attr("stroke", "steelblue")
			    .attr("stroke-dasharray", "5,5")
			    .attr("d", line);
		}
		redraw_solution([cycloid(end_point.x, end_point.y).data_points]);
		
		return container;
    }
    
    show_problem({top: 6, right: 6, bottom: 6, left: 6});
}

function func_points2(fx, fy, domain, a){
	var r = new Array, x = domain.map(fx), y = domain.map(fy);
	for(var i=0;i<domain.length;++i) r.push({t: domain[i], x:x[i]*a, y:y[i]*a});
    return r;
}

function newton(x0, f, f1){
	var t = x0, ft, ct=0;
	do{ ft = f(t); t -= ft/f1(t); } while((Math.abs(ft)>1e-3) && ++ct<20);
	console.log(`newton iters: ${ct} x0=${x0} t=${t}`);
	return t;
}

function cycloid(x0, y0){
	var fx = t=>t-Math.sin(t), fy = t=>1.0-Math.cos(t),
		f = t => fx(t)/fy(t)-(x0/y0);
	var p = {};
	p.t0 = newton(6 /*approach from the right*/, f, t => ((fy(t)*2-t*Math.sin(t))/Math.pow(fy(t), 2)));
	p.a = y0 / fy(p.t0);
	return {
		params: p,
		data_points: func_points2(fx, fy, d3.range(0, p.t0, p.t0/50.1), p.a)
	}
}

$(render_plots);
